<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    
<h3 class="main_tit">수정 이력</h3>

<div class="eas_record_pop_top">
	<a href="#eas_record_pop_compare" rel="modal:open" class="button btn_main">비교</a>
	<a href="#eas_record_pop_view" rel="modal:open" class="button btn_main">보기</a>
</div>

<!-- 기본 테이블 시작 -->
<div class="tbl_common tbl_basic eas_record_pop_tbl">
	<div class="tbl_wrap">
		<table class="tbl_ctn">
			 <colgroup>
               <col style="width: 10%;">
               <col style="width: 55%;">
               <col style="width: 20%;">
               <col style="width: 15%;">
             </colgroup>
			<thead class="eas_pop_tbl_head">
				<tr class="tbl_main_tit">
					<th></th>
					<th>처리일시</th>
					<th>구분</th>
					<th>버전</th>
				</tr>
			</thead>
			<tr>
				<td><input type="checkbox"></td>
				<td>값이 들어가면?</td>
				<td></td>
				<td></td>
			</tr>
		</table>
	</div>
</div>
<!-- 기본 테이블 끝 -->
<div class="eas_record_pop_btn_div">
<button class="btn_white"><a href="#" rel="modal:close">취소</a></button>
<button class="btn_main"><a href="#" rel="modal:close">확인</a></button>
</div>

<!-- popup include -->
<div id="eas_record_pop_view" class="modal">
	<jsp:include page="../pop/eas_record_pop_view.jsp" />
</div>

<!-- popup include -->
<div id="eas_record_pop_compare" class="modal">
	<jsp:include page="../pop/eas_record_pop_compare.jsp" />
</div>