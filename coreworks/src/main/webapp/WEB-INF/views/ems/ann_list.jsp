<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>연차관리</h2></div>
                    <!-- 탭 영역 시작 -->
                    <div>
                    	<ul class="tabs">
							<li class="tab-link current" data-tab="tab-1">전체 사원 보기</li>
							<li class="tab-link" data-tab="tab-2">내 팀원보기</li>
							
							<li class="ann_all_btn"><button class="btn_main ems_btn">일괄사용</button></li>
						</ul>
						
						<div id="tab-1" class="tab-content current">
							<!-- 테이블 위 컨텐츠 시작 -->
		                    <div class="main_cnt">
		                        <select name="" id="">
		                            <option value="">이름</option>
		                            <option value="">부서</option>
		                        </select>
		                        <input type="search" name="" id="">                      
		                        <button class="btn_solid_pink">검색</button>	                                        	                        
		                        <select name="" id="" class="ems_btn">
		                            <option value="">부서명</option>
		                            <option value="">이름</option>
		                        </select> 
		                    </div>
		                    <!-- 테이블 위 컨텐츠 끝 -->
		                    <!-- 기본 테이블 시작 -->
		                    <div class="tbl_common tbl_basic">
		                        <div class="tbl_wrap">
		                            <table class="tbl_ctn">
		                                <colgroup>
		                                    <col style="width: *;">
		                                    <col style="width: 16%;">
		                                </colgroup>
		                                <tr class="tbl_main_tit">
		                                    <th>사번</th>
		                                    <th>이름</th>
		                                    <th>직급</th>
		                                    <th>부서</th>
		                                    <th>남은 연차</th>
		                                    <th>사용 연차</th>                        
		                                </tr>
		                                <tr>
		                                    <td class="tit"><a href="../ajax/ajax.jsp" class="modal_pop" rel="modal:open">팝업예시</a></td>
		                                    <td><a href="#">내용5</a></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                </tr>
		                                <tr>
		                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
		                                    <td><a href="#">내용5</a></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                </tr>
		                                <tr>
		                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
		                                    <td><a href="#">내용5</a></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                </tr>
		                                <tr>
		                                    <td class="tit"><a href="#">내용1</a></td class="tit">
		                                    <td><a href="#">내용5</a></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                </tr>
		                            </table>
		                        </div>
		                    </div>
		                    <!-- 기본 테이블 끝 -->
		                    <!-- 페이저 시작 -->
		                    <div class="pager_wrap">
		                        <ul class="pager_cnt clearfix">
		                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
		                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
		                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
		                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
		                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
		                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
		                        </ul>
		                    </div>
		                    <!-- 페이저 끝 -->
						</div>
						<div id="tab-2" class="tab-content">
							<!-- 테이블 위 컨텐츠 시작 -->
		                    <div class="main_cnt">
		                        <select name="" id="">
		                            <option value="">이름</option>
		                            <option value="">부서</option>
		                        </select>
		                        <input type="search" name="" id="">                      
		                        <button class="btn_solid_pink">검색</button>	                                        
		                        <select name="" id="" class="ems_btn">
		                            <option value="">부서명</option>
		                            <option value="">이름</option>
		                        </select> 
		                    </div>
		                    <!-- 테이블 위 컨텐츠 끝 -->
		                    <!-- 기본 테이블 시작 -->
		                    <div class="tbl_common tbl_basic">
		                        <div class="tbl_wrap">
		                            <table class="tbl_ctn">
		                                <colgroup>
		                                    <col style="width: *;">
		                                    <col style="width: 16%;">
		                                </colgroup>
		                                <tr class="tbl_main_tit">
		                                    <th>사번</th>
		                                    <th>이름</th>
		                                    <th>직급</th>
		                                    <th>부서</th>
		                                    <th>남은 연차</th>
		                                    <th>사용 연차</th>                        
		                                </tr>
		                                <tr>
		                                    <td class="tit"><a href="../ajax/ajax.jsp" class="modal_pop" rel="modal:open">팝업예시</a></td>
		                                    <td><a href="#">내용5</a></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                    <td></td>
		                                </tr>
		                                
		                            </table>
		                        </div>
		                    </div>
		                    <!-- 기본 테이블 끝 -->
		                    <!-- 페이저 시작 -->
		                    <div class="pager_wrap">
		                        <ul class="pager_cnt clearfix">
		                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
		                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
		                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
		                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
		                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
		                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
		                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
		                        </ul>
		                    </div>
		                    <!-- 페이저 끝 -->
						</div>
                    </div>
                    <!-- 탭 영역 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(2).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });
    
    //탭 전환용 스크립트
    $(document).ready(function(){
    	
    	$('ul.tabs li').click(function(){
    		var tab_id = $(this).attr('data-tab');

    		$('ul.tabs li').removeClass('current');
    		$('.tab-content').removeClass('current');

    		$(this).addClass('current');
    		$("#"+tab_id).addClass('current');
    	})

    })

</script>
</body>
</html>