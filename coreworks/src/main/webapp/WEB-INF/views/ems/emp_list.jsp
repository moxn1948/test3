<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<jsp:include page="../inc/ems_menu.jsp" />
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_hr.css">
    
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>계정관리</h2></div>
                    <!-- 테이블 위 컨텐츠 시작 -->
                    <div class="main_cnt">
                        <select name="" id="">
                        	<option value="" hidden selected>부서선택</option>
                            <option value="">1</option>
                            <option value="">2</option>
                        </select>
                        <input type="search" name="" id="" placeholder="이름으로 검색하세요.">
                        <!-- <input type="text" name="" id="">
                        <input type="password" name="" id="">
                        <button class="btn_solid">글쓰기</button> -->
                        <button class="btn_solid_main">검색</button>
                        <!-- <button class="btn_main">검색</button>
                        <button class="btn_white">버튼</button>
                        <button class="btn_blue">버튼</button>
                        <button class="btn_pink">버튼</button>
                        <a href="#" class="button btn_solid">글쓰기</a>
                        <a href="#" class="button btn_main">a버튼</a>
                        <a href="#" class="button btn_white">a버튼</a>
                        <a href="#" class="button btn_blue">a버튼</a>
                        <a href="#" class="button btn_pink">a버튼</a> -->
                        
                        <!-- 색 있는 버튼 -->
                        <!-- <button class="btn_solid_main">버튼</button>
                        <button class="btn_solid_white">버튼</button>
                        <button class="btn_solid_blue">버튼</button>
                        <button class="btn_solid_pink">버튼</button>

                        <a href="#" class="button btn_solid_main">버튼</a>
                        <a href="#" class="button btn_solid_white">버튼</a>
                        <a href="#" class="button btn_solid_blue">버튼</a>
                        <a href="#" class="button btn_solid_pink">버튼</a> -->
                        <button class="btn_main ems_btn">사원 등록</button>
                        <button class="btn_main ems_btn">사원 일괄 등록</button>
                        <button class="btn_white ems_btn">Excel로 저장</button>
                    </div>
                    <!-- 테이블 위 컨텐츠 끝 -->
                    <!-- 기본 테이블 시작 -->
                    <div class="tbl_common tbl_basic">
                        <div class="tbl_wrap">
                            <table class="tbl_ctn">
                                <colgroup>
                                    <col style="width: *;">
                                    <col style="width: 16%;">
                                </colgroup>
                                <tr class="tbl_main_tit">
                                    <th>사번</th>
                                    <th>이름</th>
                                    <th>이메일</th>
                                    <th>입사일</th>
                                    <th>휴대전화</th>
                                    <th>직급/직책</th>                        
                                </tr>
                                <tr>
                                    <td class="tit"><a href="../ajax/ajax.jsp" class="modal_pop" rel="modal:open">팝업예시</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">뱁새가 밥을 안머거요</a></td>
                                    <td><a href="#">내용5</a></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="tit"><a href="#">내용1</a></td class="tit">
                                    <td><a href="#">내용5</a></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <!-- 기본 테이블 끝 -->
                    <!-- 페이저 시작 -->
                    <div class="pager_wrap">
                        <ul class="pager_cnt clearfix">
                        <li class="pager_com pager_arr first"><a href="javascrpt: void(0);">&#x003C;&#x003C;</a></li>
                        <li class="pager_com pager_arr prev"><a href="javascrpt: void(0);">&#x003C;</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">1</a></li>
                        <li class="pager_com pager_num on"><a href="javascrpt: void(0);">2</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">3</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">4</a></li>
                        <li class="pager_com spager_num"><a href="javascrpt: void(0);">5</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">6</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">7</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">8</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">9</a></li>
                        <li class="pager_com pager_num"><a href="javascrpt: void(0);">10</a></li>
                        <li class="pager_com pager_arr next"><a href="javascrpt: void(0);">&#x003E;</a></li>
                        <li class="pager_com pager_arr end"><a href="javascrpt: void(0);">&#x003E;&#x003E;</a></li>
                        </ul>
                    </div>
                    <!-- 페이저 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>



<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>
<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(6).addClass("on");

        // 서브 메뉴 처리
        // 열리지 않는 메뉴
        $("#menu_area .menu_list").eq(0).addClass("on");
        
        // 열리는 메뉴
        //$("#menu_area .menu_list").eq(2).addClass("on").addClass("open");
        //$("#menu_area .menu_list").eq(2).find(".sub_menu_list").eq(0).addClass("on");
    });

</script>
</body>
</html>