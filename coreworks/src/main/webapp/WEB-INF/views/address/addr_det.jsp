<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <!-- 안쓰는 페이지.. -->
<jsp:include page="../inc/address_menu.jsp" />
<!-- date picker css -->
<link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
<link rel="stylesheet" type="text/css" href="${ contextPath }/resources/css/style_mj.css">
            <div id="scroll_area" class="inner_rt">
            <!-- 메인 컨텐츠 영역 시작! -->
                <div class="main_ctn">
                    <div class="menu_tit"><h2>연락처 상세보기</h2></div>
                    <!-- ** 코드 작성부 시작 -->
                    <div class="main_cnt">
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">아이디</div>
                            <div class="main_cnt_desc">bapbap</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이름 *</div>
                            <div class="main_cnt_desc">최원준</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사명</div>
                            <div class="main_cnt_desc">softcore</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직책</div>
                            <div class="main_cnt_desc">매니저</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">직급</div>
                            <div class="main_cnt_desc">대리</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">부서</div>
                            <div class="main_cnt_desc">개발부</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">핸드폰번호</div>
                            <div class="main_cnt_desc">010-1211-1231</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">이메일</div>
                            <div class="main_cnt_desc">bap@softcore.com</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">회사주소</div>
                            <div class="main_cnt_desc">29138 서울시 강남구 역상동 C강의장</div>
                        </div>
                        <div class="main_cnt_list clearfix">
                            <div class="main_cnt_tit">기념일</div>
                            <div class="main_cnt_desc">
                                <table class="spe_day_tbl">
                                    <tr>
                                        <th>기념일명</th>
                                        <th>날짜</th>
                                        <th>양/음력</th>
                                    </tr>
                                    <tr>
                                        <td>생일</td>
                                        <td>96/12/31</td>
                                        <td>양력</td>
                                    </tr>
                                    <tr>
                                        <td>생일</td>
                                        <td>96/12/31</td>
                                        <td>양력</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                        <div class="main_cnt_list clearfix main_cnt_list_btn">
                            <button class="btn_pink">삭제</button>
                            <button class="btn_main">수정</button>
                        </div>
                    </div>
                    <!-- ** 코드 작성부 끝 -->
                </div>
            <!-- 메인 컨텐츠 영역 끝! -->
            </div><!-- inner_rt end -->
        </div>
    </main>
</div>

<!-- 주소검색 api -->
<script src="https://t1.daumcdn.net/mapjsapi/bundle/postcode/prod/postcode.v2.js"></script>
<!-- datepicker api -->
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<!-- 공통 script -->
<script type="text/javascript" src="${ contextPath }/resources/js/script.js"></script>

<script>
    $(function(){
        // 주 메뉴 분홍색 하이라이트 처리
        $("#nav .nav_list").eq(3).addClass("on");

    });

    
</script>
</body>
</html>