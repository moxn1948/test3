<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>


<div class="chat"> <!-- 채팅 div 시작 -->
	<div class="chatlogo"> <!-- 채팅창 상단 로고 div -->
		<div class="imgdiv">
			<img src="${ contextPath }/resources/images/logo_1.png" width="100px"/>
		</div>
		<div class="xbtn">
			<i class="fas fa-times"></i>
		</div>
	</div> <!-- 로고 div 끝 -->
	<div class="chatlist"> <!-- 채팅방 표시 div -->
	
		<div class="chatlistline clearfix"> <!-- 채팅방 하나하나 div --> 
			<!-- <div class="chatusericon"> 유저 사진 들어갈 자리 , 여기는 없을 때
				<i class="fas fa-user-circle"></i> 
			</div> --> <!-- 유저사진, 아이콘 div 끝 -->
			<div class="chatuserimg">
                <img src="${ contextPath }/resources/images/user_img.jpg" alt="">
            </div>
			<div class="chattext"> <!-- 발신자, 발신내용 표시할 div -->
				<p><b>황규환</b></p>
				<p>내일 회식은 역삼갈비 어떠세요?</p>
			</div> <!-- 발신자 div 끝 -->
			<div class="chattime"> <!-- 시간, 메세지 갯수 div -->
				<p>03 : 48 PM</p>
				<div class="chatcount">99</div>
			</div> <!-- 시간, 메세지 div 끝 -->
		</div> <!-- 채팅방 하나 div 끝 -->
		
		<div class="chatlistline clearfix"> <!-- 채팅방 하나하나 div --> 
			<div class="chatusericon"> <!-- 유저 사진 들어갈 자리 , 없으면 아이콘으로 -->
				<i class="fas fa-user-circle"></i> 
			</div> <!-- 유저사진, 아이콘 div 끝 -->
			<div class="chattext"> <!-- 발신자, 발신내용 표시할 div -->
				<p><b>황규환</b></p>
				<p>내일 회식은 역삼갈비 어떠세요?</p>
			</div> <!-- 발신자 div 끝 -->
			<div class="chattime"> <!-- 시간, 메세지 갯수 div -->
				<p>03 : 48 PM</p>
				<div class="chatcount">100</div>
			</div> <!-- 시간, 메세지 div 끝 -->
		</div> <!-- 채팅방 하나 div 끝 -->
		
		<div class="chatlistline clearfix"> <!-- 채팅방 하나하나 div --> 
			<div class="chatusericon"> <!-- 유저 사진 들어갈 자리 , 없으면 아이콘으로 -->
				<i class="fas fa-user-circle"></i> 
			</div> <!-- 유저사진, 아이콘 div 끝 -->
			<div class="chattext"> <!-- 발신자, 발신내용 표시할 div -->
				<p><b>황규환</b></p>
				<p>내일 회식은 역삼갈비 어떠세요?</p>
			</div> <!-- 발신자 div 끝 -->
			<div class="chattime"> <!-- 시간, 메세지 갯수 div -->
				<p>03 : 48 PM</p>
				<div class="chatcount">100</div>
			</div> <!-- 시간, 메세지 div 끝 -->
		</div> <!-- 채팅방 하나 div 끝 -->
		
		<div class="chatlistline clearfix"> <!-- 채팅방 하나하나 div --> 
			<div class="chatusericon"> <!-- 유저 사진 들어갈 자리 , 없으면 아이콘으로 -->
				<i class="fas fa-user-circle"></i> 
			</div> <!-- 유저사진, 아이콘 div 끝 -->
			<div class="chattext"> <!-- 발신자, 발신내용 표시할 div -->
				<p><b>황규환</b></p>
				<p>내일 회식은 역삼갈비 어떠세요?</p>
			</div> <!-- 발신자 div 끝 -->
			<div class="chattime"> <!-- 시간, 메세지 갯수 div -->
				<p>03 : 48 PM</p>
				<div class="chatcount">100</div>
			</div> <!-- 시간, 메세지 div 끝 -->
		</div> <!-- 채팅방 하나 div 끝 -->
		
		<div class="chatlistline clearfix"> <!-- 채팅방 하나하나 div --> 
			<div class="chatusericon"> <!-- 유저 사진 들어갈 자리 , 없으면 아이콘으로 -->
				<i class="fas fa-user-circle"></i> 
			</div> <!-- 유저사진, 아이콘 div 끝 -->
			<div class="chattext"> <!-- 발신자, 발신내용 표시할 div -->
				<p><b>황규환</b></p>
				<p>내일 회식은 역삼갈비 어떠세요?</p>
			</div> <!-- 발신자 div 끝 -->
			<div class="chattime"> <!-- 시간, 메세지 갯수 div -->
				<p>03 : 48 PM</p>
				<div class="chatcount">100</div>
			</div> <!-- 시간, 메세지 div 끝 -->
		</div> <!-- 채팅방 하나 div 끝 -->
		
		<div class="chatlistline clearfix"> <!-- 채팅방 하나하나 div --> 
			<div class="chatusericon"> <!-- 유저 사진 들어갈 자리 , 없으면 아이콘으로 -->
				<i class="fas fa-user-circle"></i> 
			</div> <!-- 유저사진, 아이콘 div 끝 -->
			<div class="chattext"> <!-- 발신자, 발신내용 표시할 div -->
				<p><b>황규환</b></p>
				<p>내일 회식은 역삼갈비 어떠세요?</p>
			</div> <!-- 발신자 div 끝 -->
			<div class="chattime"> <!-- 시간, 메세지 갯수 div -->
				<p>03 : 48 PM</p>
				<div class="chatcount">100</div>
			</div> <!-- 시간, 메세지 div 끝 -->
		</div> <!-- 채팅방 하나 div 끝 -->
		
		<div class="chatlistline clearfix"> <!-- 채팅방 하나하나 div --> 
			<div class="chatusericon"> <!-- 유저 사진 들어갈 자리 , 없으면 아이콘으로 -->
				<i class="fas fa-user-circle"></i> 
			</div> <!-- 유저사진, 아이콘 div 끝 -->
			<div class="chattext"> <!-- 발신자, 발신내용 표시할 div -->
				<p><b>황규환</b></p>
				<p>내일 회식은 역삼갈비 어떠세요?</p>
			</div> <!-- 발신자 div 끝 -->
			<div class="chattime"> <!-- 시간, 메세지 갯수 div -->
				<p>03 : 48 PM</p>
				<div class="chatcount">100</div>
			</div> <!-- 시간, 메세지 div 끝 -->
		</div> <!-- 채팅방 하나 div 끝 -->
		
		
	</div> <!-- 채팅방 div 끝 -->


	<div class="lastdiv"> <!-- 마지막 색으로 이루어진 div -->
		<div class="chatsetting"> <!-- 설정 아이콘 -->
			<i class="fas fa-cog"></i>
		</div>
		<div class="newchatbtn"> <!-- 새 채팅 아이콘 -->
			<i class="fas fa-comment"></i>
		</div>
	</div>
</div>

<script>
<%-- 상단 x버튼 클릭시 채팅목록 닫기 --%>
$(".chatlogo .xbtn").click(function() { 
	$(".chat").css("display","none");
});

<%-- 채팅목록 더블클릭시 채팅창 열기 --%>
$(".chatlistline").dblclick(function() {
	console.log($(this));
	
	$("#chatForm").css("display","block");
});
</script>